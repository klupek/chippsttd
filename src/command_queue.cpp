/* $Id: command_queue.cpp 4998 2006-05-30 16:15:17Z Frostregen $ */

#include "stdafx.h"
#include "tile_type.h"
#include "command_type.h"
#include "command_func.h"
#include "debug.h"
#include "copy_paste.h"
#include "command_queue.h"
#include "settings_type.h"
#include "gfx_func.h"
#include "date_func.h"

CopyPasteCommandQueue::~CopyPasteCommandQueue()
{
	this->ClearCopyPasteCommandQueue();
}

/** Enqueue the given command.
 * May be executed later by calling ExecuteNextCommand()
 */
void CopyPasteCommandQueue::CopyPasteQueueCommand(TileIndex tile, uint32 p1, uint32 p2, CommandCallback *callback, uint32 cmd)
{
	CommandContainer *cc = new CommandContainer;
	
	cc->tile = tile;
	cc->p1   = p1;
	cc->p2   = p2;
	cc->callback = callback;
	cc->cmd  = cmd;
	
	/* add it to the queue */
	this->queue.push(cc);
}

/** Executes the next queued command, or does nothing if queue is emtpy
 * Since DoCommandP checks for _shift_pressed,
 * we have to store, unset und restore it.
 */
void CopyPasteCommandQueue::ExecuteNextCommand()
{
	/* Check if queue is not empty */
	if (!this->queue.empty()) {
		/* backup and unset _shift_pressed */
		bool temp_shift_pressed = _shift_pressed;
		_shift_pressed = false;
		/* Get the command and execute it */
		DoCommandP(this->queue.front());
		/* restore _shift_pressed */
		_shift_pressed = temp_shift_pressed;
		/* delete this commandcontainer */
		{
			CommandContainer *cc = this->queue.front();
			this->queue.pop();
			delete cc;
		}
	}
}

/** Clears the CopyPasteCommandQueue
 */
void CopyPasteCommandQueue::ClearCopyPasteCommandQueue()
{
	while (!this->queue.empty()) {
		this->queue.pop();
	}
}

void CallCopyPasteCommandQueueTick()
{
	/* Process the command_queue */
	if (_settings_client.gui.cp_paste_speed < 255) {
		if ((_tick_counter % _settings_client.gui.cp_paste_speed) == 0) {
			_copy_paste_command_queue.ExecuteNextCommand();
		}
	}
}
