/* $Id: tunnel_map.cpp 21273 2010-11-20 15:44:24Z alberth $ */

/*
 * This file is part of OpenTTD.
 * OpenTTD is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 2.
 * OpenTTD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with OpenTTD. If not, see <http://www.gnu.org/licenses/>.
 */

/** @file tunnel_map.cpp Map accessors for tunnels. */

#include "stdafx.h"
#include "tunnelbridge_map.h"
#include "station_map.h"


/**
 * Gets the other end of the tunnel. Where a vehicle would reappear when it
 * enters at the given tile.
 * @param tile the tile to search from.
 * @return the tile of the other end of the tunnel.
 */
TileIndex GetOtherTunnelEnd(TileIndex tile)
{
	DiagDirection dir = GetTunnelBridgeDirection(tile);
	TileIndexDiff delta = TileOffsByDiagDir(dir);
	uint z = GetTileZ(tile);

	dir = ReverseDiagDir(dir);
	do {
		tile += delta;
	} while (
		!IsTunnelTile(tile) ||
		GetTunnelBridgeDirection(tile) != dir ||
		GetTileZ(tile) != z
	);

	return tile;
}

/**
 * Helper function for under_water tunnel finding.
 * Only one strech of water is allowed to be passed.
 * @param tile Tile to be validated.
 
 * @return true if first srtech of water is being crossed else false.
 */
bool IsWaterCrossingTunnel(TileIndex tile, DiagDirection dir)
{
	TileIndexDiff delta = TileOffsByDiagDir(dir);
	int height;
	/* Initialize amount of tiles allowed to be passed backwards to find crossing water tunnel entrance.
	 * and stops iteration to save time. */
	int tiles = 5;

	do {
		tile -= delta;
		if (!IsValidTile(tile)) return false;
		height = GetTileZ(tile);
		tiles--;
	} while (height > 0 && tiles > 0);

	return height == 0 && IsTunnelTile(tile) && GetTunnelBridgeDirection(tile) == dir;
}

/**
 * Is there a tunnel in the way in the given direction?
 * @param tile the tile to search from.
 * @param z    the 'z' to search on.
 * @param dir  the direction to start searching to.
 * @return true if and only if there is a tunnel.
 */
bool IsTunnelInWayDir(TileIndex tile, uint z, DiagDirection dir)
{
	TileIndexDiff delta = TileOffsByDiagDir(dir);
	uint height;

	do {
		tile -= delta;
		if (!IsValidTile(tile)) return false;
		height = GetTileZ(tile);
	} while (z < height ||
			(height == 0 &&
			GetTileMaxZ(tile) == 0 &&	// Flat tile.
			(IsTileType(tile, MP_WATER) || IsBuoyTile(tile))));

	if (z == height) {
		if (IsTunnelTile(tile)) return GetTunnelBridgeDirection(tile) == dir;

		if (height == 0) {
			DiagDirection direction_slope = GetInclinedSlopeDirection(GetTileSlope(tile, NULL));
			if (direction_slope == dir || direction_slope == ReverseDiagDir(dir)) {
				return IsWaterCrossingTunnel(tile, ReverseDiagDir(direction_slope));
			}
		}
	}
	return false;
}

/**
 * Is there a tunnel in the way in any direction?
 * @param tile the tile to search from.
 * @param z the 'z' to search on.
 * @return true if and only if there is a tunnel.
 */
bool IsTunnelInWay(TileIndex tile, uint z)
{
	return IsTunnelInWayDir(tile, z, (TileX(tile) > (MapMaxX() / 2)) ? DIAGDIR_NE : DIAGDIR_SW) ||
			IsTunnelInWayDir(tile, z, (TileY(tile) > (MapMaxY() / 2)) ? DIAGDIR_NW : DIAGDIR_SE);
}

bool IsTunnelBelow(TileIndex tile)
{
	uint32 z = GetTileZ(tile);
	while (z > 0) {
		z -= TILE_HEIGHT;
		if (IsTunnelInWay(tile, z)) return true;
	}
	return false;
}
