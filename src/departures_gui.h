/* $Id: departures_gui.h $ */

/*
 * This file is part of OpenTTD.
 * OpenTTD is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 2.
 * OpenTTD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with OpenTTD. If not, see <http://www.gnu.org/licenses/>.
 */

/** @file departures_gui.h */

#ifndef DEPARTURES_GUI_H
#define DEPARTURES_GUI_H

#include "departures_type.h"
#include "station_base.h"

/** Enum for DeparturesView, referring to _nested_departures_list */
enum DeparturesWindowWidgets {
	DW_WIDGET_CAPTION,		///< Window caption
	DW_WIDGET_LIST,			///< List of departures
	DW_WIDGET_SCROLLBAR,	///< List scrollbar
	DW_SHOW_DEPS,           ///< Toggle departures button
	DW_SHOW_ARRS,           ///< Toggle arrivals button
	DW_SHOW_VIA,            ///< Toggle via button
	DW_SHOW_TRAINS,         ///< Toggle trains button
	DW_SHOW_ROADVEHS,       ///< Toggle road vehicles button
	DW_SHOW_SHIPS,          ///< Toggle ships button
	DW_SHOW_PLANES,         ///< Toggle planes button
};

void ShowStationDepartures(StationID station);
void ShowWaypointDepartures(StationID waypoint);

#endif /* DEPARTURES_GUI_H */
