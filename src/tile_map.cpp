/* $Id: tile_map.cpp 20632 2010-08-26 22:01:16Z rubidium $ */

/*
 * This file is part of OpenTTD.
 * OpenTTD is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 2.
 * OpenTTD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with OpenTTD. If not, see <http://www.gnu.org/licenses/>.
 */

/** @file tile_map.cpp Global tile accessors. */

#include "stdafx.h"
#include "tile_map.h"

static Slope GetTileSlopeGivenHeight(uint hnorth, uint hwest, uint heast, uint hsouth, uint *h) {

	/* Due to the fact that tiles must connect with each other without leaving gaps, the
	 * biggest difference in height between any corner and 'min' is between 0, 1, or 2.
	 *
	 * Also, there is at most 1 corner with height difference of 2. */

	uint hminnw = min(hnorth, hwest);
	uint hmines = min(heast, hsouth);
	uint hmin = min(hminnw, hmines);

	uint hmaxnw = max(hnorth, hwest);
	uint hmaxes = max(heast, hsouth);
	uint hmax = max(hmaxnw, hmaxes);

	uint r = SLOPE_FLAT; ///< Computed slope of the tile.

	if (hnorth != hmin) {
		r += SLOPE_N;
	}
	if (hwest != hmin) {
		r += SLOPE_W;
	}
	if (heast != hmin) {
		r += SLOPE_E;
	}
	if (hsouth != hmin) {
		r += SLOPE_S;
	}
	if (hmax - hmin == 2) {
		r += SLOPE_STEEP;
	}

	if (h != NULL) *h = hmin * TILE_HEIGHT;

	return (Slope)r;
}

/**
 * Return the slope of a given tile inside the map.
 * @param tile Tile to compute slope of
 * @param h    If not \c NULL, pointer to storage of z height
 * @return Slope of the tile, except for the HALFTILE part
 */
Slope GetTileSlope(TileIndex tile, uint *h)
{
	assert(tile < MapSize());

	if (TileX(tile) == MapMaxX() || TileY(tile) == MapMaxY() ||
			(_settings_game.construction.freeform_edges && (TileX(tile) == 0 || TileY(tile) == 0))) {
		if (h != NULL) *h = TileHeight(tile) * TILE_HEIGHT;
		return SLOPE_FLAT;
	}

	uint hnorth = TileHeight(tile);                    // Height of the North corner.
	uint hwest = TileHeight(tile + TileDiffXY(1, 0));  // Height of the West corner.
	uint heast = TileHeight(tile + TileDiffXY(0, 1));  // Height of the East corner.
	uint hsouth = TileHeight(tile + TileDiffXY(1, 1)); // Height of the South corner.

	return GetTileSlopeGivenHeight(hnorth, hwest, heast, hsouth, h);
}

/**
 * Return the slope of a given tile outside the map.
 *
 * @param tile Tile outside the map to compute slope of.
 * @param h    If not \c NULL, pointer to storage of z height.
 * @return Slope of the tile outside map, except for the HALFTILE part. */
Slope GetTileSlopeOutsideMap(int x, int y, uint *h)
{
	uint hnorth = TileHeightOutsideMap(x, y);         // N corner.
	uint hwest = TileHeightOutsideMap(x + 1, y);      // W corner.
	uint heast = TileHeightOutsideMap(x, y + 1);      // E corner.
	uint hsouth = TileHeightOutsideMap(x + 1, y + 1); // S corner.

	return GetTileSlopeGivenHeight(hnorth, hwest, heast, hsouth, h);
}

/**
 * Get bottom height of the tile inside the map.
 * @param tile Tile to compute height of
 * @return Minimum height of the tile
 */
uint GetTileZ(TileIndex tile)
{
	if (TileX(tile) == MapMaxX() || TileY(tile) == MapMaxY()) return 0;

	uint h = TileHeight(tile);                       // N corner.
	h = min(h, TileHeight(tile + TileDiffXY(1, 0))); // W corner.
	h = min(h, TileHeight(tile + TileDiffXY(0, 1))); // E corner.
	h = min(h, TileHeight(tile + TileDiffXY(1, 1))); // S corner.

	return h * TILE_HEIGHT;
}

/**
 * Get bottom height of the tile outside the map.
 *
 * @param tile Tile outside the map to compute height of.
 * @return Minimum height of the tile outside map. */
uint GetTileZOutsideMap(int x, int y)
{
	uint h = TileHeightOutsideMap(x, y);            // N corner.
	h = min(h, TileHeightOutsideMap(x + 1, y));     // W corner.
	h = min(h, TileHeightOutsideMap(x, y + 1));     // E corner.
	h = min(h, TileHeightOutsideMap(x + 1, y + 1)); // S corner.

	return h * TILE_HEIGHT;
}

/**
 * Get top height of the tile inside the map.
 * @param t Tile to compute height of
 * @return Maximum height of the tile
 */
uint GetTileMaxZ(TileIndex t)
{
	if (TileX(t) == MapMaxX() || TileY(t) == MapMaxY()) return TileHeightOutsideMap(TileX(t), TileY(t));

	uint h = TileHeight(t); // N corner
	h = max(h, TileHeight(t + TileDiffXY(1, 0))); // W corner
	h = max(h, TileHeight(t + TileDiffXY(0, 1))); // E corner
	h = max(h, TileHeight(t + TileDiffXY(1, 1))); // S corner

	return h * TILE_HEIGHT;
}

/**
 * Get top height of the tile the outside map.
 *
 * @see Detailed description in header.
 *
 * @param tile Tile outside to compute height of.
 * @return Maximum height of the tile.
 */
uint GetTileMaxZOutsideMap(int x, int y)
{
	uint h = TileHeightOutsideMap(x, y);
	h = max(h, TileHeightOutsideMap(x + 1, y));
	h = max(h, TileHeightOutsideMap(x, y + 1));
	h = max(h, TileHeightOutsideMap(x + 1, y + 1));

	return h * TILE_HEIGHT;
}
