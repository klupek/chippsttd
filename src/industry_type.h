/* $Id: industry_type.h 19812 2010-05-13 09:44:44Z rubidium $ */

/*
 * This file is part of OpenTTD.
 * OpenTTD is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 2.
 * OpenTTD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with OpenTTD. If not, see <http://www.gnu.org/licenses/>.
 */

/** @file industry_type.h Types related to the industry. */

#ifndef INDUSTRY_TYPE_H
#define INDUSTRY_TYPE_H

typedef uint16 IndustryID;
typedef uint16 IndustryGfx;
typedef uint8 IndustryType;
struct Industry;

struct IndustrySpec;
struct IndustryTileSpec;

static const IndustryID INVALID_INDUSTRY = 0xFFFF;

static const IndustryType NEW_INDUSTRYOFFSET     = 37;                ///< original number of industries
static const IndustryType NUM_INDUSTRYTYPES      = 64;                ///< total number of industries, new and old
static const IndustryType INVALID_INDUSTRYTYPE   = NUM_INDUSTRYTYPES; ///< one above amount is considered invalid

static const IndustryGfx  INDUSTRYTILE_NOANIM    = 0xFF;              ///< flag to mark industry tiles as having no animation
static const IndustryGfx  NEW_INDUSTRYTILEOFFSET = 175;               ///< original number of tiles
static const IndustryGfx  NUM_INDUSTRYTILES      = 512;               ///< total number of industry tiles, new and old
static const IndustryGfx  INVALID_INDUSTRYTILE   = NUM_INDUSTRYTILES; ///< one above amount is considered invalid

static const int INDUSTRY_COMPLETED = 3; ///< final stage of industry construction.

/**
 * When building a new industry, the game tries to level an appropriate place,
 * if and only if there is no need to change the height of any tile by more
 * than some given number of heightlevels. This number is defined here.
 * Why do we need values > 1? Because when generating very rough maps, the game
 * may run out of appropriate places for industries.
 * And finally, to prevent industries at unrealistic heightlevels, we apply
 * this extra logic only if we are below the heightlevel set in Advanced settings->Economy->Industries.
 */
enum IndustryMaxLevelling {
	IND_MAX_LEVELLING_NORMAL         = 1,  ///< Usual behaviour
	IND_MAX_LEVELLING_VERY_ROUGH     = 3,  ///< When generating a very rough map using TerraGenesis
	IND_MAX_LEVELLING_CEREALLY_ROUGH = 4,  ///< When generating a cereally rough map using TerraGenesis
};

#endif /* INDUSTRY_TYPE_H */
