/* $Id: command_queue.h 4998 2006-05-30 16:13:41Z Frostregen $ */

#ifndef COMMAND_QUEUE_H
#define COMMAND_QUEUE_H

#include "map_type.h"
#include "command_type.h"
#include "settings_type.h"
#include <queue>

class CopyPasteCommandQueue {
private:
	std::queue<CommandContainer *> queue;

public:
	~CopyPasteCommandQueue();

	void ClearCopyPasteCommandQueue();        ///< Clears the CommandQueue

	void CopyPasteQueueCommand(TileIndex tile, uint32 p1, uint32 p2, CommandCallback *callback, uint32 cmd); ///< Enqueue the given command.
	void ExecuteNextCommand();       ///< Executes the next queued command, or does nothing if queue is emtpy
};

extern CopyPasteCommandQueue _copy_paste_command_queue;

void CallCopyPasteCommandQueueTick();

#endif /* COMMAND_QUEUE_H */
